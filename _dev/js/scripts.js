//INICIA ONLOAD
document.addEventListener("DOMContentLoaded", function() {

//Sticky Menú
var stickyElement = document.getElementById("stickyElement");
var stuck = false;
var stickPoint = getDistance();

function getDistance() {
  var topDist = stickyElement.offsetTop;
  return topDist;
}

window.onscroll = function(e) {
  var distance = getDistance() - window.pageYOffset;
  var offset = window.pageYOffset;
  if ( (distance <= 0) && !stuck) {
    stickyElement.style.position = 'fixed';
    stickyElement.style.top = '0px';
    stuck = true;
  } else if (stuck && (offset <= stickPoint)){
    stickyElement.style.position = 'static';
    stuck = false;
  }
}

//Menu

// var iMenu = document.querySelector('.menu');
// var mMenu = document.querySelector('#multi-menu');
// var sMenu = document.querySelector('.submenu a');
// var sMenu2 = document.querySelector('.submenu ul');

// iMenu.onclick = function() {
// 	iMenu.classList.toggle('open');
// 	mMenu.classList.toggle('mOpen');
// }
// sMenu.onclick = function() {
// 	sMenu2.classList.toggle('sOpen');
// }




// //Scroll Spy
// var spy = new Gumshoe('.mMenu a', {

// 	// Active classes
// 	navClass: 'active', // applied to the nav list item
// 	contentClass: 'none', // applied to the content

// 	// Nested navigation
// 	nested: false, // if true, add classes to parents of active link
// 	nestedClass: 'active', // applied to the parent items

// 	// Offset & reflow
// 	offset: 0, // how far from the top of the page to activate a content area
// 	reflow: false, // if true, listen for reflows

// 	// Event support
// 	events: true // if true, emit custom events

// });


// Smooth Scroll
var scroll = new SmoothScroll('a[href*="#"]', {
	speed: 500,
	speedAsDuration: true
});



//- OffSide
var offsideMenu1 = offside( '#menu-1', {
	slidingElementsSelector: '.slideOff',
	debug: false,
	buttonsSelector: '.menu-btn-1, .menu-btn-1--close',
	slidingSide: 'left',
	beforeOpen: function(){},
	beforeClose: function(){},
});
var overlay = document.querySelector('.site-overlay')
	.addEventListener( 'click', offside.factory.closeOpenOffside );
console.log(offsideMenu1);



//-END ONLOAD
});