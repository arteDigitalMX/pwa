'use strict'

const {src, dest, watch, series} = require('gulp'),
	sass = require('gulp-sass'),
	postcss = require('gulp-postcss'),
	cssnano = require('cssnano'),
	terser = require('gulp-terser'),
	imagemin = require('gulp-imagemin'),
	pngquant = require('imagemin-pngquant'),
	imageminMozjpeg = require('imagemin-mozjpeg'),
	server = require('gulp-express')


// Sass Task
function scssTask(){
	return src('./_dev/scss/**/*.scss')
	.pipe(sass({
		outputStyle: 'compact'
	}))
	.pipe(postcss([cssnano()]))
	.pipe(dest('./public/src/css'))
}

// Javascript Task
function jsTask(){
	return src('./_dev/js/**/*.js')
	.pipe(terser())
	.pipe(dest('./public/src/js'))
}

//  Images Task
function imgTask(){
	return src('./public/src/img/**/*')
	.pipe(imagemin([
		pngquant({quality: [0.5, 0.8]}),
		imageminMozjpeg([50])
	]))
	.pipe(dest('./public/src/img/'))
}

// Server Task
function serverTask(cb) {
    // Start the server at the beginning of the task
    server.run(['server.js'])
 
    // Restart the server when file changes
	watch(['./**/*'], server.run)
	cb()
}



// Watch Task
function watchTask(){
	watch(['./_dev/scss/**/*.scss', './_dev/js/**/*.js', './_dev/views/**/*.pug'], series(scssTask, jsTask, imgTask))
}

// Default Gulp Task
exports.default = series(
	scssTask,
	jsTask,
	imgTask,
	serverTask,
	watchTask
)

