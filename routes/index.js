'use strict'

var express = require('express'),
	router = express.Router()

function index(req,res,next){
	let locals = {
		title : 'arteDigitalMX',
		link : 'https://arte-digital.mx',
		description : 'El arte del desarrollo web'
	}
	res.render('index', locals)
}

function jade(req,res,next){
	let locals = {
		title : 'arteDigitalMX',
		link : 'https://arte-digital.mx',
		description : 'El arte del desarrollo web'
	}
	res.render('jade', locals)
}

function error404(req,res,next){
	let error = Error(),
		locals = {
			title : 'Error 404',
			description : 'Recurso no encontrado',
			error : error
		}

	error.status = 404
	res.render('error', locals)

	next()
}

router
	.get('/', index)
	.get('/jade', jade)
	.use(error404)

module.exports = router