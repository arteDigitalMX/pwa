'use strict'

var express = require('express'),
	favicon = require('serve-favicon'),
	pug = require('pug'),
	routes = require('./routes/index'),
	faviconURL = `${__dirname}/public/favicon.png`,
	publicDir = express.static(`${__dirname}/public/`),
	viewDir = `${__dirname}/_dev/views`,
	port = (process.env.PORT || 3000),
	app = express()

app
	// Configurando App
	.set('views', viewDir)
	.set('view engine', 'pug')
	.set('port', port)
	// Ejecuntando middleware
	.use(favicon(faviconURL))
	.use(publicDir)
	// Ejecutando middleware enrutador
	.use('/', routes)




module.exports = app